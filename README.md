# TradersClub

A simple **CRUD** app for the TradersClub API.

# Introduction

I was going to use [Next.js](https://github.com/zeit/next.js/) initially, but due some issues with its SSR and unique way to get initial props, I decided to use vanilla React - specifically the [`create-react-app`](https://github.com/facebook/create-react-app) boilerplate from Facebook.

## Motivations and approaches

I tried to use the latest on both React and CSS/HTML. For instance, instead of ES6 Classes, all but one component are Functional Components, which are [faster](https://twitter.com/trueadm/status/916706152976707584) and easier to create a better [*separation of concerns*](https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0). There's a lot of articles regarding this issue, but I like [this one](https://medium.com/@learnreact/container-components-c0e67432e005) and is the way I've been using so far. This section would be incomplete without mentioning the [`recompose`](https://github.com/acdlite/recompose) package, which makes handling of states, handlers and much more *so* much easier.

All of the above combined with the use of [HOCs](https://reactjs.org/docs/higher-order-components.html), really makes React a joy to code.

As for HTML and CSS, I've tried to use the latest and where possible semantic way to create the components. Here's some articles I found relevant:

+ [We Write CSS Like We Did in the 90s, and Yes, It’s Silly](https://alistapart.com/article/we-write-css-like-we-did-in-the-90s-and-yes-its-silly)
+ [MDN CSS grid](https://developer.mozilla.org/en-US/docs/Web/CSS/grid)

---
# Possible caveats

For some reason the `production` API results are **not** showing up.  
The response payload for the HTTP actions `POST, PUT, DELETE` are returning as expected from the documentation, however `GET`ting (list) all the cars seems down.

I added a mock API address and left it commented out.

### UPDATE:
`POST`, aka, creating a new car is _not_ working as expected, the payload response for the API is just the `id`.  
Using cURL:  
```
$ curl 'http://tchml.tradersclub.com.br:12000/api/cars' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0' -H 'Accept: */*' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Referer: http://localhost:3000/' -H 'content-type: application/json' -H 'origin: http://localhost:3000' -H 'DNT: 1' -H 'Connection: keep-alive' --data '{"title":"Teste","model":"Teste","brand":"Chevrolet","year":1123,"color":"Branco","km":123123,"price":123123}'
```
Response: `{"car":{"id":14}}`

---

## Using the application

Since this repository is based on `create-react-app` its better to follow the instructions from themselves. But the basics:

1. Clone the repository:
```
$ git clone https://gitlab.com/trinaldi/tradersclub
```

2. Go to the newly created folder
```
$ cd path/to/folder
```
3. `yarn` install the packages
```
$ yarn install
```
4. Test drive it
```
$ yarn start
```

Below some guides from `create-react-app`
+ [Available Scripts section](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#available-scripts)
+ [Deploy section](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#deployment)

---
## Relevant literature

+ [Why The Hipsters Recompose Everything](https://medium.com/javascript-inside/why-the-hipsters-recompose-everything-23ac08748198)
+ [Building HOCs with Recompose](https://medium.com/front-end-developers/building-hocs-with-recompose-7debb951d101)
+ [Top 5 Recompose HOCs](https://medium.com/@abhiaiyer/top-5-recompose-hocs-1a4c9cc4566)


## Feedback wanted!