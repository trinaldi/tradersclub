// https://stackoverflow.com/questions/4912788/truncate-not-round-off-decimal-numbers-in-javascript
const toFixedDown = function(value, digits) {
  var re = new RegExp("(\\d+\\.\\d{" + digits + "})(\\d)"),
    m = value.toString().match(re);
  return m ? parseFloat(m[1]) : value.valueOf();
};

function padWithZeros(v) {
  var s = v.toString();
  if (s.indexOf(".") === -1) s += ".";
  while (s.length < s.indexOf(".") + 3) s += "0";
  return s;
}

function currencyFormat(v) {
  if (typeof v === "undefined" || v === null) return "";
  if (v === 0) {
    return "0.00";
  }
  try {
    v = parseFloat(v);
    v = padWithZeros(toFixedDown(v, 2))
      .toString()
      .replace(".", ",");
    return v.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
  } catch (e) {
    console.error(e);
    return "";
  }
}

export default currencyFormat;