import React from "react";
import Select from "react-virtualized-select";


import "react-select/dist/react-select.css";
import "react-virtualized-select/styles.css";

const Details = (props) => {
  const {
    loading,
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    setFieldValue,
    handleBlur,
    handleSubmit,
    handleReset,
    newBrand,
    setNewBrand,
    removeCar,
    onRemoveCar,
  } = props;
  
  return(
    <form className="form" onSubmit={handleSubmit}>
    {removeCar && <p style={{fontWeight: "700"}}>Carro removido com sucesso!</p>}
    {props.status && props.status.edited && <p style={{fontWeight: "700"}}>Carro editado com sucesso!</p>}
    {props.status && props.status.created && <p style={{fontWeight: "700"}}>Carro criado com sucesso!</p>}
    <input name="title" type="text"
      value={values.title} 
      onChange={handleChange}
      onBlur={handleBlur}
      placeholder="Nome do carro" />
    {errors.title && touched.title && <div className="invalid-feedback">{errors.title}</div>}
  <div className="form-group">
  <div>
    <input name="model" type="text" 
      value={values.model} 
      onChange={handleChange}
      onBlur={handleBlur}
      placeholder="Modelo" />
    {errors.model && touched.model && <div className="invalid-feedback">{errors.model}</div>}
    </div>
    <div>
    <input name="year" type="text" 
      value={values.year} 
      onChange={handleChange}
      onBlur={handleBlur}
      pattern="[0-9]{0,4}"
      title="O ano deve conter 4 dígitos"
      maxLength={4}
      placeholder="Ano" />
    {errors.year && touched.year && <div className="invalid-feedback">{errors.year}</div>}
    </div>
  </div>
  {!loading && values.brands &&  (
    <Select
      classNamePrefix="brand-select"
      name="brand"
      options={values.brands}
      isClearable={false}
      isSearchable={false}
      value={newBrand}
      placeholder="Marca..."
      onChange={(e) => {setNewBrand(e.value); setFieldValue('brand', e.label)}}
      /> )}
    {errors.brand && touched.brand && <div className="invalid-feedback">{errors.brand}</div>}
  <div className="form-group">
  <div>
    <input name="color" type="text" 
      value={values.color} 
      onChange={handleChange}
      placeholder="Cor do carro"
      onBlur={handleBlur} />
    {errors.color && touched.color && <div className="invalid-feedback">{errors.color}</div>}
    </div>
    <div>
    <input name="km" type="text" 
      value={values.km} 
      onChange={handleChange}
      placeholder="Quilometragem"
      pattern="[0-9]+(,[0-9]{0,2})?"
      title="O preço deve estar no padrão: 111,00"
      onBlur={handleBlur} />
    {errors.km && touched.km && <div className="invalid-feedback">{errors.km}</div>}
    </div>
  </div>
    <input name="price" type="text" 
      value={values.price} 
      onChange={handleChange}
      placeholder="Preço"
      pattern="[0-9]+(,[0-9]{0,2})?"
      title="O preço deve estar no padrão: 111,00"
      onBlur={handleBlur} />
    {errors.price && touched.price && <div className="invalid-feedback">{errors.price}</div>}
  <div className="bottom-btns">
  {values.id !== "" && <input type="button"
    onClick={() => onRemoveCar(values)}
    value={isSubmitting ? "Carregando..." : "Remover"}
    className="btn"
    disabled={isSubmitting} />}
  {values.id === "" && <input type="button"
    onClick={handleReset}
    value={isSubmitting ? "Carregando..." : "Cancelar"}
    className="btn"
    disabled={isSubmitting} />
  }
  <div></div>
  <button type="submit" className="btn active" disabled={isSubmitting}>
    {isSubmitting ? "Carregando..." : "Salvar"}
  </button>
  </div>
</form>
  )
}

export default Details;