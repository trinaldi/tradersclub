import Details from "./Details";
import { withFormik } from "formik";
import { withState, withHandlers, compose } from "recompose";
import Yup from "yup";

const changeKey= (originalKey, newKey, brands) => {
  let newArr = [];
  for(let i = 0; i < brands.length; i++)
  {
    let obj = brands[i];
    obj[newKey] = obj[originalKey];
    delete(obj[originalKey]);
    newArr.push(obj);
  }
  return newArr;
}

let brands = []
fetch('http://tchml.tradersclub.com.br:12000/api/brands')
// fetch('http://private-anon-7be4f2a24b-tradersclubapi.apiary-mock.com/api/brands')
.then(response => response.json())
.then(data => {
  brands = data.brands
  changeKey("id", "value", brands)
  changeKey("name", "label", brands)
  return brands
});


const enhance = compose(
  withState("newBrand", "setNewBrand", typeof(props) === 'undefined' ? props => props.brandId : ""),
  withState("removeCar", "setRemoveCar", false),
  withFormik({
    mapPropsToValues: (props) => ({
      id: props.id || "",
      title: props.title || "",
      model: props.model || "",
      brand: props.brand || "",
      brands: props.brands || brands,
      year: props.year || "",
      color: props.color || "",
      km: props.km || "",
      price: props.price || "",
    }),

    validationSchema: Yup.object().shape({
      title: Yup.string().required("Insira o nome do carro"),
      model: Yup.string().required("Insira o modelo do carro"),
      brand: Yup.string().required("Insira a marca do carro"),
      year: Yup.string()
        .required("Insira o ano do carro"),
      color: Yup.string().required("Insira a cor do carro"),

      // https://duvidas.dicio.com.br/quilometragem-ou-kilometragem/
      km: Yup.string()
      .required("Insira a quilometragem do carro"),

      price: Yup.string()
        .required("Insira o preço do carro"),
    }),
    mapValuesToPayload: x => x,
    handleSubmit: (values, { setStatus, setSubmitting }) => {
      if(values.id === "") {
        setTimeout(() => {
          const { brands, brandId, id, ...noBrands } = values
          const unmaskedBody = ({
            ...noBrands,
            price: noBrands.price.toString().replace(/,/g, "."),
            km: noBrands.km.toString().replace(/,/g, ".")
          })
          fetch(`http://tchml.tradersclub.com.br:12000/api/cars`,{
            method: 'POST',
            headers: {
              "Content-Type": "application/json",
              'Cache-Control': 'no-cache'
            },
            body: JSON.stringify({car: unmaskedBody})
          })
          .then(response => response.json())
          .then(setStatus({ created: true }))
            setSubmitting(false)
        }, 1000);
      } else {
        setTimeout(() => {
          const { brands, brandId, ...noBrands } = values
          const unmaskedBody = ({
            ...noBrands,
            price: noBrands.price.toString().replace(/,/g, "."),
            km: noBrands.km.toString().replace(/,/g, ".")
          })
          fetch(`http://tchml.tradersclub.com.br:12000/api/cars/${noBrands.id}`,{
            method: 'PUT',
            headers: {
              "Content-Type": "application/json",
              'Cache-Control': 'no-cache'
            },
            body: JSON.stringify({car: unmaskedBody})
          })
          .then(response => response.json())
          .then(setStatus({ edited: true }))
            setSubmitting(false)
        }, 1000);
      }
    },
  }),
  withHandlers({
      onRemoveCar: props => car => {
        const { brands, ...noBrands } = car
        fetch(`http://tchml.tradersclub.com.br:12000/api/cars/${noBrands.id}`,{
          method: 'DELETE',
          headers: {
            "Content-Type": "application/json",
            'Cache-Control': 'no-cache'
          }
        })
        .then(response => response.text())
        props.setRemoveCar(!props.removeCar)
      }
  })
);


export default enhance(Details);
