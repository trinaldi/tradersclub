import React from "react";
import Car from "./components/Car";
import Details from "../Details";

const changeKey= (originalKey, newKey, arr) => {
  let newArr = [];
  for(let i = 0; i < arr.length; i++)
  {
    let obj = arr[i];
    obj[newKey] = obj[originalKey];
    delete(obj[originalKey]);
    newArr.push(obj);
  }
  return newArr;
}

let arr = []
fetch("http://tchml.tradersclub.com.br:12000/api/brands")
//fetch("https://private-anon-7be4f2a24b-tradersclubapi.apiary-mock.com/api/brands")
.then(function(response) { 
  return response.json();
}).then(function(j) {
  arr = j.brands
  changeKey("id", "value", arr)
  changeKey("name", "label", arr)
  arr.push({"value": 7, "label": "Artesão feudal"})
  return arr
});
  




const CarsList = ({
  carId,
  setCarId,
  
  cars
}) => (
  <section className="cars-dialog">
  {!carId && cars
  .filter(r => r.title)
  .map(r => {
    let car = {brands: arr, ...r}
    let brandId = car.brands.find(c => c.value && r.brand === c.label)
    let newCar = {brandId,brands: arr, ...r}
    return (
      <div key={r.id} onClick={() => setCarId(newCar)}>
        <Car key={r.id}  car={r} />
      </div>
    )
  })
  }
  {
    carId && <Details  {...carId}/>
  }
</section>
  );

export default CarsList