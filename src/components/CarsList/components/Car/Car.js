import React from "react";
import currencyFormat from "../../../../utils/currencyFormat";
import kmFormat from "../../../../utils/kmFormat";

const Car = ({
  onCarDetail,
  car
}) => (
  <div className="car-container" id={car.id}>
    <p>{car.title}</p>
    <p>
    {`${car.model} ‧ 
    ${car.brand} ‧ 
    ${car.year} ‧ 
    ${kmFormat(car.km)}`}</p>
    <p>{`R$ ${currencyFormat(car.price)}`}</p>
    <p>{car.year}</p>
  </div>
)

export default Car