import TopBar from "./TopBar";
import { withState, withHandlers, compose } from "recompose";

const enhance = compose(
  withState("search", "setSearch", ""),
  withState("register", "setRegister", false),
  withHandlers({
    onSearchCars: props => car => {
      fetch(`http://tchml.tradersclub.com.br:12000/api/cars?search=${car}`)
      //fetch(`http://private-anon-7be4f2a24b-tradersclubapi.apiary-mock.com/api/cars?search=${car}`)
      .then(response => response.json())
      .then(data => {
        car !== "" ? props.setSearch(data) : props.setSearch("")
      })
      .catch(error => console.error(error))
    }
  })

)

export default enhance(TopBar);